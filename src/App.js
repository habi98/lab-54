import React, { Component, Fragment } from 'react';
import './App.css';

import Card from './componetnts/Card/Card';
import CardDeck from "./CardDeck";

class App extends Component {
    constructor () {
        super();
        this.state = {
            cards: []
        };
    }

  clickHandler = () => {
      const cards = new CardDeck();
      this.setState({
          cards: cards.getCards(5)
      });
  };

  render() {

    return (
        <Fragment>
            <div className="block-btn">
                <button onClick={this.clickHandler} className="btn-add"  type='button'>change deck</button>
            </div>

            {this.state.cards.map((card) => {
                return (
                    <Card suit={card.suit} rank={card.rank} />
                )
            })}
        </Fragment>
    )
  }
}

export default App;
