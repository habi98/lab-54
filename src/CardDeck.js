import React from 'react';


class CardDeck {
    constructor(){
        this.cards = {
            rank: ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J','Q', 'K', 'A',],
            suit: ['S', 'H', 'C' ,'D'],
        };
        this.allCards = [];
        for(let i = 0; i < this.cards.rank.length; i++) {
            for(let j = 0; j < this.cards.suit.length; j++) {
                const card = {rank: this.cards.rank[i], suit: this.cards.suit[j]};
                this.allCards.push(card)
            }
        }
    }

    getCard() {
        const index = Math.floor(Math.random() * this.allCards.length);
        const randCard = this.allCards[index];
        this.allCards.splice(index, 1);
        return randCard;
    }

    getCards (howMany) {
        const fiveCards = [];
        for(let i = 0; i < howMany; i++) {
            fiveCards.push(this.getCard())
        }
        return fiveCards;
    }


}

export default CardDeck;