import React from 'react'

import './Card.css'

const Card = (props) => {

    let suit = '';



    let rank = '';
    const suits = {
        'D': '♦',
        'C': '♣',
        'H': '♥',
        'S': '♠'

    };

    switch(props.suit) {
        case 'D': suit = 'diams'
            break;
        case 'H' : suit = 'hearts'
            break;
        case 'C' : suit = 'clubs'
            break;
        case 'S' : suit = 'spades'
            break
    };
     switch (props.rank){
         case '2' : rank = '2';
             break;
         case '3' : rank = '3';
             break;
         case '4' : rank = '4';
             break;
         case '5' : rank = '5';
             break;
         case '6' : rank = '6';
             break;
         case '7' : rank = '7';
             break;
         case '8' : rank = '8';
             break;
         case '9' : rank = '9';
             break;
         case '10' : rank = '10';
             break;
         case 'J' : rank = 'j';
             break;
         case 'Q' : rank = 'q';
             break;
         case 'K' : rank = 'k';
             break;
         case 'A' : rank = 'a';
             break;
     }

    return(
           <div className={`Card Card-${suit} Card-rank-${rank}`}>
               <span className="Card-rank">{props.rank}</span>
               <span className="Card-suit">{suits[props.suit]}</span>
           </div>
    )
};

export default Card;